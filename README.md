Quickly run a tinyDNS server by passing in routes as CLI arguments

ex:
   docker run --rm -ti -p 53:53/udp dns .bar.com:10.10.23.2 =foo.bar.com:10.10.23.2
   nslookup foo.bar.com 127.0.0.1