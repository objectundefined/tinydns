FROM alpine:3.4

RUN apk add --no-cache tinydns drill

ENV ROOT="/data/" IP="0.0.0.0" UID="0" GID="0"
EXPOSE 53

WORKDIR /data

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["tinydns"]
